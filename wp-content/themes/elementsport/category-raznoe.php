<?php get_header(); ?>
    <script>$(function(){$('.l-raz').addClass('active');});</script>
    <main>
        <div class="glob_coll-left">
            <?php get_sidebar('left') ?>
        </div>
        <div class="glob_coll-right">
            <div class="main-cont main_pg-post">
                <?php get_sidebar('search_and_comm') ?>

                <?php if(have_posts('category__in=4&showposts=6')):  ?> <!-- Если есть посты -->

                    <div class="columns-2">
                        <div class="column_1">
                            <?php $i = 1; ?>
                            <?php while(have_posts()) : the_post(); $i++;?> <!-- Запускаем цикл перебора постов -->
                                <?php if ($i % 2) continue; ?>

                                <?php $category = get_the_category($post->ID)[0]; ?>

                                <div class="post">
                                    <!--                        <a href="--><?//= get_category_link($category->cat_ID)?><!--" style="background-image: url('--><?//= get_template_directory_uri();?><!--/images/--><?//= $category->cat_ID; ?><!--.png')" class="post_cat"></a>-->
                                    <div class="wr-post-header">
                                        <i class="small-line"></i>
                                        <img src="<?= get_template_directory_uri();?>/images/cat_img-<?= $category->cat_ID; ?>.png" />
                                        <h2>
                                            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                        </h2>
                                        <p class="post_time">
                                            <?php the_time('j l в H:i'); ?>  <b>|</b>  <span>Раздел:</span> <a href="<?= get_category_link($category->cat_ID)?>" class=""><?php echo get_cat_name($category->cat_ID);?></a>
                                        </p>
                                    </div>

                                    <a href="<?php the_permalink(); ?>" class="post_link-img">
                                        <?php the_post_thumbnail(); ?>
                                    </a>
                                    <div class="post_prev-text">
                                        <!-- добовление короткого описание производится в админке (вверху справа, настройки екрана)-->
                                        <?php the_excerpt(); ?>
                                    </div>
                                </div>


                            <?php endwhile; ?>
                        </div>

                        <div class="column_2">
                            <?php $i = 0; ?>
                            <?php while(have_posts()) : the_post(); $i++;?> <!-- Запускаем цикл перебора постов -->
                                <?php if ($i % 2) continue; ?>

                                <?php $category = get_the_category($post->ID)[0]; ?>

                                <div class="post">
                                    <!--                        <a href="--><?//= get_category_link($category->cat_ID)?><!--" style="background-image: url('--><?//= get_template_directory_uri();?><!--/images/--><?//= $category->cat_ID; ?><!--.png')" class="post_cat"></a>-->
                                    <div class="wr-post-header">
                                        <i class="small-line"></i>
                                        <img src="<?= get_template_directory_uri();?>/images/cat_img-<?= $category->cat_ID; ?>.png" />
                                        <h2>
                                            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                        </h2>
                                        <p class="post_time">
                                            <?php the_time('j l в H:i'); ?>  <b>|</b>  <span>Раздел:</span> <a href="<?= get_category_link($category->cat_ID)?>" class=""><?php echo get_cat_name($category->cat_ID);?></a>
                                        </p>
                                    </div>

                                    <a href="<?php the_permalink(); ?>" class="post_link-img">
                                        <?php the_post_thumbnail(); ?>
                                    </a>
                                    <div class="post_prev-text">
                                        <!-- добовление короткого описание производится в админке (вверху справа, настройки екрана)-->
                                        <?php the_excerpt(); ?>
                                    </div>
                                </div>


                            <?php endwhile; ?>
                        </div>
                    </div>

                    <div class="columns-1">
                        <?php while(have_posts()) : the_post(); ?> <!-- Запускаем цикл перебора постов -->

                            <?php $category = get_the_category($post->ID)[0]; ?>

                            <div class="post">
                                <!--                        <a href="--><?//= get_category_link($category->cat_ID)?><!--" style="background-image: url('--><?//= get_template_directory_uri();?><!--/images/--><?//= $category->cat_ID; ?><!--.png')" class="post_cat"></a>-->
                                <div class="wr-post-header">
                                    <i class="small-line"></i>
                                    <img src="<?= get_template_directory_uri();?>/images/cat_img-<?= $category->cat_ID; ?>.png" />
                                    <h2>
                                        <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                    </h2>
                                    <p class="post_time">
                                        <?php the_time('j l в H:i'); ?>  <b>|</b>  <span>Раздел:</span> <a href="<?= get_category_link($category->cat_ID)?>" class=""><?php echo get_cat_name($category->cat_ID);?></a>
                                    </p>
                                </div>

                                <a href="<?php the_permalink(); ?>" class="post_link-img">
                                    <?php the_post_thumbnail(); ?>
                                </a>
                                <div class="post_prev-text">
                                    <!-- добовление короткого описание производится в админке (вверху справа, настройки екрана)-->
                                    <?php the_excerpt(); ?>
                                </div>
                            </div>
                        <?php endwhile; ?>
                        <!-- Автоматическая подгрузка постов 1 vs 3 -->
                        <?php if (  $wp_query->max_num_pages > 1 ) : ?>
                            <script>
                                var ajaxurl = '<?php echo site_url() ?>/wp-admin/admin-ajax.php';
                                var true_posts = '<?php echo serialize($wp_query->query_vars); ?>';
                                var current_page = <?php echo (get_query_var('paged')) ? get_query_var('paged') : 1; ?>;
                                var max_pages = '<?php echo $wp_query->max_num_pages; ?>';
                            </script>
                            <div id="load_more_gs">
                                <div class="cssload-container"><div class="cssload-whirlpool"></div></div>
                            </div>
                        <?php endif; ?>
                    </div>


                <?php endif; ?>
                <div class="wr-up">
                    <a href="#up" class="up">
                        <img src="<?= get_template_directory_uri();?>/images//up.svg" alt="images">
                    </a>
                </div>

            </div>
            <div class="glob-sidebar">
                <?php get_sidebar('right') ?>
            </div>

        </div>

    </main>
<?php get_footer(); ?>