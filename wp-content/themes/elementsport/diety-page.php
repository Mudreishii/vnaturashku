<?php
/*
Template Name: Диеты Сборная
*/
get_header(); ?>
    <!--<script>$(function(){$('.l-home').addClass('active');});</script>-->
<main>
    <div class="glob_coll-left">
        <?php get_sidebar('left'); ?>
    </div>
    <div class="glob_coll-right">
        <div class="main-cont main_pg-post">
            <?php get_sidebar('search_and_comm') ?>
            <?php if( function_exists('kama_breadcrumbs') ) kama_breadcrumbs(); ?>
            <?php
            global $post;

            // записываем $post во временную переменную $tmp_post
            $tmp_post = $post;
            $args = array( 'post_type' => 'page', 'category' => 14, 'posts_per_page' => 100 );
            $category = get_the_category($post->ID)[0];
            $myposts = get_posts( $args );

            foreach( $myposts as $post ){ setup_postdata($post);
                ?>
                <div class="post">
                    <!--                        <a href="--><?//= get_category_link($category->cat_ID)?><!--" style="background-image: url('--><?//= get_template_directory_uri();?><!--/images/--><?//= $category->cat_ID; ?><!--.png')" class="post_cat"></a>-->
                    <div class="wr-post-header">
                        <i class="small-line"></i>
                        <img src="<?= get_template_directory_uri();?>/images/cat_img-<?= $category->cat_ID; ?>.png" />
                        <h2>
                            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                        </h2>
                        <p class="post_time">
                            <?php the_time('j l в H:i'); ?>  |  <span>Раздел:</span> <a href="<?= get_category_link($category->cat_ID)?>" class=""><?php echo get_cat_name($category->cat_ID);?></a>
                        </p>
                    </div>

                    <a href="<?php the_permalink(); ?>" class="post_link-img">
                        <?php the_post_thumbnail(); ?>
                    </a>
                    <div class="post_prev-text">
                        <!-- добовление короткого описание производится в админке (вверху справа, настройки екрана)-->
                        <?php the_excerpt(); ?>
                    </div>
                </div>
            <?php
            }

            // возвращаем былое значение $post
            $post = $tmp_post;
            ?>
        </div>
        <div class="glob-sidebar">
            <?php get_sidebar('right'); ?>
        </div>
    </div>

</main>
<?php get_footer(); ?>