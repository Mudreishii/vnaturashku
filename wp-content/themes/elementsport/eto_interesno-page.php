<?php
/*
Template Name: Это интересно Шаблон
*/
get_header(); ?>
    <script>$(function(){$('.l-home').addClass('active');});</script>
    <main>
        <div class="glob_coll-left">
            <?php get_sidebar('left'); ?>
        </div>
        <div class="glob_coll-right">
            <div class="main-cont main_pg-post">
                <?php if(have_posts()):  ?> <!-- Если есть посты -->

                    <?php while(have_posts()) : the_post();?> <!-- Запускаем цикл перебора постов -->

                        <?php $category = get_the_category($post->ID)[0]; ?>

                        <div class="post post-full manual">
                            <h1>
                                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                            </h1>

                            <?php

                            echo(get_post_meta($post->ID, 'title', true));
                            ?>
                            <!--                            <a href="--><?php //the_permalink(); ?><!--" class="post_link-img">-->
                            <!--                                --><?php //the_post_thumbnail(); ?>
                            <!--                            </a>-->
                            <div class="post_prev">
                                <!-- добовление короткого описание производится в админке (вверху справа, настройки екрана)-->
                                <?php the_content(); ?>
                            </div>
                        </div>

                    <?php endwhile; ?>

                <?php endif; ?>
            </div>
            <div class="glob-sidebar">
                <?php get_sidebar('right'); ?>
            </div>
        </div>

    </main>
<?php get_footer(); ?>