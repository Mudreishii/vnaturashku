<!DOCTYPE html>
<html <?php language_attributes(); ?> style="margin-top: 0px !important;">
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <title><?php wp_title(); ?></title>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/fonts.css">
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/style.css">

    <script src="<?php echo get_template_directory_uri(); ?>/js/jquery-3.1.1.slim.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/jquery-2.2.4.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/yuser-javascript.js"></script>

    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
    <div class="zaglushka"></div>

    <header id="up">
        <div class="wr-categor-solinks">
            <div class="wr-categor">
                <a href="<?php bloginfo('url'); ?>" class="l-home">Главная</a>
                <b>Разделы:</b>
                <a href="/category/video" class="l-video">Видео</a>
                <a href="/category/instagramm" class="l-insta">Инстаграмм</a>
                <a href="/category/krossfit" class="l-kross">Кроссфит</a>
                <a href="/category/raznoe" class="l-raz">Разное</a>
                <a href="/category/recipes" class="l-rec">Рецепты</a>
            </div>
            <div class="wr-solinks">
                <span>Наши группы:</span>
                <a href="">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/vk_logo.png" alt="images">
                </a>
                <a href="">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/facebook_logo.png" alt="images">
                </a>
                <a href="">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/twitter_logo.png" alt="images">
                </a>
            </div>
            <div class="disp-on-mob"></div>
        </div>
    </header>