$(function(){
  var a = localStorage.getItem('elementsport');
  console.log(a);
  if(a) {
    $('.zaglushka').remove();
  }

  $('.wr-categor .big b').click(function(){
    $('.wr-categor .wr').slideToggle(500);
  });

  // Появление скролла
  $(window).scroll(function(){
    if($(window).scrollTop() >= 106){
      $('a.up').fadeIn();
    } else {
      $('a.up').fadeOut();
    }
    if($(window).scrollTop() >= 159){
      $('.glob_coll-right .glob-sidebar .wr-interesting').addClass('fix');
    } else {
      $('.glob_coll-right .glob-sidebar .wr-interesting').removeClass('fix');
    }
  });

  // Плавный скролл
  $(document).ready(function() {
    $("a.up").click(function() {
      var elementClick = $(this).attr("href")
      var destination = $(elementClick).offset().top;
      jQuery("html:not(:animated),body:not(:animated)").animate({
        scrollTop: destination
      }, 800);
      return false;
    });
  });

  // Включить-Выключить коментарии
//  $('.last-comment .switcher').click(function(){
//    if($(this).hasClass('off')){
//      $(this).removeClass('off').addClass('on');
//    } else {
//      $(this).removeClass('on').addClass('off');
//    }
//  });

  // Начальное разделение постов на 1 / 2 колонки
  $(document).ready(function(){
    if($(window).width() < 1350 ){
      $('.columns-2').hide();
      $('.columns-1').show();
    } else {
      $('.columns-1').hide();
      $('.columns-2').show();
    }
  });
  $(window).resize(function(){
    if($(window).width() < 1350 ){
      $('.columns-2').hide();
      $('.columns-1').show();
    } else {
      $('.columns-1').hide();
      $('.columns-2').show();
    }
  });

  <!-- Автоматическая подгрузка постов 2 vs 3 -->
  var lastload = 0;
  var newload = 0;
  $(window).scroll(function(){
    var bottomOffset = 1200;
    var data = {
      'action': 'loadmore',
//      'query': true_posts,
      'page' : current_page
    };
    if( $(document).scrollTop() > ($(document).height() - bottomOffset) && !$('body').hasClass('loading')){

      $.ajax({
        url:ajaxurl,
        data:data,
        type:'POST',
        beforeSend: function( xhr){
          newload = data.page;
          if(newload != lastload){
            lastload = newload;
            $('body').addClass('loading');
          }
        },
        success:function(data){

          if( data ) {

//            if($(window).width() > 1350){
              var el = $.parseHTML(data);
              var count = 1;

              $.each(el, function(num, elem){
                if(num %2 != 0) {
                  if(count == 1){
                    $('.column_1').append(el[num]);
                    count = 0;
                  } else {
                    $('.column_2').append(el[num]);
                    count = 1;
                  }
                }
              });

//            } else {
//              $('.column_1').append(data);
//              $('.column_2').remove();
//            }
            if($(window).width() < 1350 ){
              $('#load_more_gs').before(data);
            }

            $('body').removeClass('loading');
            current_page++;
          }
          else {
            $('body').removeClass('loading');
          }
        }
      });
    }
  });


});