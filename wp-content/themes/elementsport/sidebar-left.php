<div class="wr-link lg">
    <a href="<?php bloginfo('url'); ?>">
        <img src="<?php echo get_template_directory_uri(); ?>/images/logo.png"/>
    </a>
</div>
<div class="wr-link">
    <a href="/kak-nakachat/" class="item icon-1">Как накачать</a>
    <div class="menu-hide">
        <a href="/kak-nakachat/bitseps"><span>Бицепс</span><b></b></a>
        <a href="/kak-nakachat/trapetsiyu"><span>Трапецию</span><b></b></a>
        <a href="/kak-nakachat/plechi"><span>Плечи</span><b></b></a>
        <a href="/kak-nakachat/grud"><span>Грудь</span><b></b></a>
        <a href="/kak-nakachat/spinu"><span>Спину</span><b></b></a>
        <a href="/kak-nakachat/ruki"><span>Руки</span><b></b></a>
        <a href="/kak-nakachat/tritseps"><span>Трицепс</span><b></b></a>
        <a href="/kak-nakachat/predplechya"><span>Предплечья</span><b></b></a>
        <a href="/kak-nakachat/press"><span>Пресс</span><b></b></a>
        <a href="/kak-nakachat/nogi"><span>Ноги</span><b></b></a>
        <a href="/kak-nakachat/yagoditsy"><span>Ягодицы</span><b></b></a>
        <a href="/kak-nakachat/ikry"><span>Икры</span><b></b></a>
    </div>
</div>
<div class="wr-link">
    <a href="/uprazhneniya/" class="item icon-2">Упражнения</a>
    <div class="menu-hide">
        <a href="/kak-nakachat/bitseps"><span>На бицепс</span><b></b></a>
        <a href="/uprazhneniya/trapetsiyu/"><span>На трапецию</span><b></b></a>
        <a href="/kak-nakachat/plechi"><span>На плечи</span><b></b></a>
        <a href="/kak-nakachat/grud"><span>На грудь</span><b></b></a>
        <a href="/kak-nakachat/spinu"><span>На спину</span><b></b></a>
        <a href="/kak-nakachat/ruki"><span>На руки</span><b></b></a>
        <a href="/kak-nakachat/tritseps"><span>На трицепс</span><b></b></a>
        <a href="/kak-nakachat/predplechya"><span>На предплечья</span><b></b></a>
        <a href="/kak-nakachat/press"><span>На пресс</span><b></b></a>
        <a href="/kak-nakachat/nogi"><span>На ноги</span><b></b></a>
        <a href="/kak-nakachat/yagoditsy"><span>На ягодицы</span><b></b></a>
        <a href="/kak-nakachat/ikry"><span>На икры</span><b></b></a>
    </div>
</div>
<div class="wr-link">
    <a href="/kak-nakachat/" class="item icon-3">Программы</a>
    <div class="menu-hide">
        <a href="/kak-nakachat/bitseps"><span>На массу</span><b></b></a>
        <a href="/kak-nakachat/trapetsiyu"><span>На силу</span><b></b></a>
        <a href="/kak-nakachat/plechi"><span>На сушку</span><b></b></a>
        <a href="/kak-nakachat/grud"><span>На поддержание</span><b></b></a>
        <a href="/kak-nakachat/spinu"><span>Тренировки дома</span><b></b></a>
        <a href="/kak-nakachat/ruki"><span>Тренировки знаменитостей</span><b></b></a>
    </div>
</div>
<div class="wr-link">
    <a href="/kak-nakachat/" class="item icon-8">Питание</a>
    <div class="menu-hide">
        <a href="/kak-nakachat/bitseps"><span>На массу</span><b></b></a>
        <a href="/kak-nakachat/trapetsiyu"><span>На сушке</span><b></b></a>
        <a href="/kak-nakachat/plechi"><span>На меж-сезон</span><b></b></a>
        <a href="/kak-nakachat/grud"><span>Калорийность продуктов</span><b></b></a>
        <a href="/kak-nakachat/spinu"><span>Простые и сложные углеводы</span><b></b></a>
        <a href="/kak-nakachat/ruki"><span>Гликемический индекс</span><b></b></a>
    </div>
</div>
<div class="wr-link">
    <a href="/kak-nakachat/" class="item icon-4">Добавки</a>
    <div class="menu-hide">
        <a href="/kak-nakachat/bitseps"><span>Общее положение о добавках</span><b></b></a>
        <a href="/kak-nakachat/trapetsiyu"><span>Витамины для спорта</span><b></b></a>
        <a href="/kak-nakachat/plechi"><span>Трибулус(tr/al)</span><b></b></a>
        <a href="/kak-nakachat/grud"><span>Эврикома</span><b></b></a>
        <a href="/kak-nakachat/spinu"><span>Икариин</span><b></b></a>
        <a href="/kak-nakachat/ruki"><span>Экдистерон</span><b></b></a>
        <a href="/kak-nakachat/bitseps"><span>Фосфатидная кислота</span><b></b></a>
        <a href="/kak-nakachat/tritseps"><span>Диабетон МВ</span><b></b></a>
        <a href="/kak-nakachat/predplechya"><span>Лив-52</span><b></b></a>
        <a href="/kak-nakachat/press"><span>Аскорбиновая кислота</span><b></b></a>
        <a href="/kak-nakachat/nogi"><span>Ионизированный кальций</span><b></b></a>
        <a href="/kak-nakachat/yagoditsy"><span>Глиатилин</span><b></b></a>
    </div>
</div>
<div class="wr-link">
    <a href="/diety/" class="item icon-5">Диеты</a>
    <div class="menu-hide">
        <a href="/diety/osnovy-lyuboj-diety/"><span>Основы любой диеты</span><b></b></a>
        <a href="/kak-nakachat/trapetsiyu"><span>Кето диета</span><b></b></a>
        <a href="/kak-nakachat/plechi"><span>БУЧ диета</span><b></b></a>
        <a href="/kak-nakachat/grud"><span>Диета Дюкана</span><b></b></a>
    </div>
</div>
<div class="wr-link">
    <a href="/zhenskij-trening/" class="item icon-6">Женский тренинг</a>
    <div class="menu-hide">
        <a href="/diety/osnovy-lyuboj-diety/"><span>Как не перекачаться девушке</span><b></b></a>
        <a href="/kak-nakachat/trapetsiyu"><span>Можно ли заниматься при менструации?</span><b></b></a>
        <a href="/kak-nakachat/plechi"><span>Можно ли увеличить грудь упражнениями</span><b></b></a>
        <a href="/kak-nakachat/grud"><span>Особенности женских тренировок</span><b></b></a>
        <a href="/zhenskij-trening/programmy-trenirovok/"><span>Программы тренировок</span><b></b></a>
    </div>
</div>
<div class="splitter"></div>
<div class="wr-link">
    <a href="/kak-nakachat/" class="item icon-7">О проекте</a>
</div>