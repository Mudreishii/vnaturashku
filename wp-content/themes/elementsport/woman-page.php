<?php
/*
Template Name:  Женский тренинг Сборная
*/

get_header(); ?>
    <main>
        <p class="back-text">SPORT</p>
        <div class="box_main">
            <div class="col-left">
                <div class="sb_about">
                    <a href="#">Описание проекта</a>
                </div>
                <div class="sb_interest">
                    <p class="sb_name">Это интерестно</p>
                    <a href="#">Кто такие натуралы, или что такое натуральный ББ</a>
                    <a href="#">Вред спортивного питания, миф или реальность?</a>
                    <a href="#">У качков не стоит?</a>
                    <a href="#">Большой не значит сильный</a>
                    <a href="#">Чем больше шкаф, тем меньше у вас шансов</a>
                    <a href="#">Качки не поворотливы?</a>
                    <a href="#">Химик или натурал, кем быть лучше?</a>
                    <a href="#">А что вы понимаете под словом Химия в ББ?</a>
                </div>

            </div>
            <div class="col-center">
                <div class="box-before-post">
                    <a href="<?php bloginfo('url'); ?>" class="on-the-main">На главную</a>
                </div>

                <?php
                global $post;

                // записываем $post во временную переменную $tmp_post
                $tmp_post = $post;
                $args = array( 'post_type' => 'page', 'category' => 9, 'posts_per_page' => 100 );
                $category = get_the_category($post->ID)[0];
                $myposts = get_posts( $args );
                foreach( $myposts as $post ){ setup_postdata($post);
                    ?>
                    <div class="post">
                        <!--                        <a href="--><?//= get_category_link($category->cat_ID)?><!--" style="background-image: url('--><?//= get_template_directory_uri();?><!--/images/--><?//= $category->cat_ID; ?><!--.png')" class="post_cat"></a>-->
                        <div class="wr-post-header">
                            <i class="small-line"></i>
                            <img src="<?= get_template_directory_uri();?>/images/cat_img-<?= $category->cat_ID; ?>.png" />
                            <h2>
                                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                            </h2>
                            <p class="post_time">
                                <?php the_time('j l в H:i'); ?>  |  <span>Раздел:</span> <a href="<?= get_category_link($category->cat_ID)?>" class=""><?php echo get_cat_name($category->cat_ID);?></a>
                            </p>
                        </div>

                        <a href="<?php the_permalink(); ?>" class="post_link-img">
                            <?php the_post_thumbnail(); ?>
                        </a>
                        <div class="post_prev-text">
                            <!-- добовление короткого описание производится в админке (вверху справа, настройки екрана)-->
                            <?php the_excerpt(); ?>
                        </div>
                    </div>
                <?php
                }

                // возвращаем былое значение $post
                $post = $tmp_post;
                ?>


            </div>
            <div class="col-right">

                <div class="sb_category">
                    <p class="sb_name">Категории</p>
                    <?php get_sidebar('right'); ?>
                </div>
                <div class="sb_vk">
                    <a href="#">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/logo_circle.png" alt="images" width="52" height="52" class="logo-circle">
                        <span>Подпишись на наше сообщество в vk!</span>
                        <img src="<?php echo get_template_directory_uri(); ?>/images/vk_line.png" alt="images" width="85" height="50" class="vk-line">
                    </a>
                </div>
                <div class="sb_inter-news">
                    <p class="sb_name">Новости интернета</p>
                </div>
                <div class="sb_music">
                    <p class="sb_name">Музыка</p>
                </div>
            </div>

        </div>
    </main>
<?php get_footer(); ?>