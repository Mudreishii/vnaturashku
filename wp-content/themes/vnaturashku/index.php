<?php get_header(); ?>
      <main>
        <div class="container">
          <div class="row">
            <div class="col-md-9">
              <h1>Выводим статьи</h1>

                <?php if(have_posts()): ?>
                  <?php while(have_posts()) : the_post(); ?>
                    <section class="wr-post">

                      <h2>
                        <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                        <?php the_date_xml(); ?>
                      </h2>
                      <div class="wr-prev-img">
                          <?php the_post_thumbnail(); ?>
                      </div>

                      <?php the_excerpt(); ?>

                      <?php the_category(); ?>

                      <a href="<?php the_permalink(); ?>">Читать далее</a>

                    </section>
                  <?php endwhile; ?>


                  <?php the_posts_pagination(); ?>

                <?php endif; ?>
              
            </div>
            <div class="col-md-3">
              <?php get_sidebar(); ?>
            </div>
          </div>
        </div>
      </main>
<?php get_footer(); ?>