<?php
	
	// Register Sidebar
	function striped_sidebar() {
	 
		$args = array(
			'id'            => 'sidebar-left',
			'name'          => __( 'Sidebar', 'striped' ),
			'description'   => __( 'Left Sidebar', 'striped' ),
			'class'         => 'striped-widget',
			'before_title'  => '<h2 class="widgettitle">',
			'after_title'   => '</h2>',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
		);
		register_sidebar( $args );
	 
	}
	// Hook into the 'widgets_init' action
	add_action( 'widgets_init', 'striped_sidebar' );
	

	if ( function_exists( 'add_theme_support' ) ) { 
		add_theme_support( 'post-thumbnails' ); 
		add_image_size( 'full-thumbnail', 650, 250, true ); // название, ширина, высота, жесткая обрезка
	}
?>