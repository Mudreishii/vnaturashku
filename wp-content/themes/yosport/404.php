<?php get_header(); ?>
    <main>
        <p class="back-text">SPORT</p>
        <div class="box_main">
            <div class="col-left">
                <div class="sb_about">
                    <a href="#">Описание проекта</a>
                </div>
                <div class="sb_interest">
                    <p class="sb_name">Это интерестно</p>
                    <a href="#">Кто такие натуралы, или что такое натуральный ББ</a>
                    <a href="#">Вред спортивного питания, миф или реальность?</a>
                    <a href="#">У качков не стоит?</a>
                    <a href="#">Большой не значит сильный</a>
                    <a href="#">Чем больше шкаф, тем меньше у вас шансов</a>
                    <a href="#">Качки не поворотливы?</a>
                    <a href="#">Химик или натурал, кем быть лучше?</a>
                    <a href="#">А что вы понимаете под словом Химия в ББ?</a>
                </div>

            </div>
            <div class="col-center">
                <div class="box-before-post">
                    <a href="<?php bloginfo('url'); ?>" class="on-the-main">На главную</a>
                </div>

                <h1>Такой страницы не существует</h1>

            </div>
            <div class="col-right">

                <div class="sb_category">
                    <p class="sb_name">Категории</p>
                    <?php get_sidebar(); ?>
                </div>
                <div class="sb_vk">
                    <a href="#">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/logo_circle.png" alt="images" width="52" height="52" class="logo-circle">
                        <span>Подпишись на наше сообщество в vk!</span>
                        <img src="<?php echo get_template_directory_uri(); ?>/images/vk_line.png" alt="images" width="85" height="50" class="vk-line">
                    </a>
                </div>
                <div class="sb_inter-news">
                    <p class="sb_name">Новости интернета</p>
                </div>
                <div class="sb_music">
                    <p class="sb_name">Музыка</p>
                </div>
            </div>

        </div>
    </main>
<?php get_footer(); ?>