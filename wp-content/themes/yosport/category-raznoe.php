<?php get_header(); ?>
    <main>
        <p class="back-text">SPORT</p>
        <div class="box_main">
<!--            <div class="col-left">-->
<!--                <div class="sb_about">-->
<!--                    <a href="#">Описание проекта</a>-->
<!--                </div>-->
<!--                <div class="sb_interest">-->
<!--                    <p class="sb_name">Это интерестно</p>-->
<!--                    <a href="#">Кто такие натуралы, или что такое натуральный ББ</a>-->
<!--                    <a href="#">Вред спортивного питания, миф или реальность?</a>-->
<!--                    <a href="#">У качков не стоит?</a>-->
<!--                    <a href="#">Большой не значит сильный</a>-->
<!--                    <a href="#">Чем больше шкаф, тем меньше у вас шансов</a>-->
<!--                    <a href="#">Качки не поворотливы?</a>-->
<!--                    <a href="#">Химик или натурал, кем быть лучше?</a>-->
<!--                    <a href="#">А что вы понимаете под словом Химия в ББ?</a>-->
<!--                </div>-->
<!--            </div>-->
            <div class="col-center pg-category">
                <div class="box-before-post">
                    <div class="last-comment">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/news.png" alt="images" width="18" height="15" class="last-comm-icon">
                        <span>Последнии коментарии</span>
                        <div class="switcher off">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/click_news.png" alt="images" class="sw-off">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/click_news_on.png" alt="images" class="sw-on">
                        </div>
                    </div>
                </div>

                <?php
                    if ( function_exists('yoast_breadcrumb') ) {
                        yoast_breadcrumb('<p id="breadcrumbs">','</p>');
                    }
                ?>

                <?php if(have_posts('category__in=4&showposts=100')):  ?> <!-- Если есть посты -->
                    <div class="column_1">
                        <?php $i = 1; ?>
                        <?php while(have_posts()) : the_post(); $i++;?> <!-- Запускаем цикл перебора постов -->
                            <?php if ($i % 2) continue; ?>

                            <?php $category = get_the_category($post->ID)[0]; ?>

                            <div class="post <?php echo $a ?>">
                                <a href="<?= get_category_link($category->cat_ID)?>" style="background-image: url('<?= get_template_directory_uri();?>/images/<?= $category->cat_ID; ?>.png')" class="post_cat"></a>
                                <h2>
                                    <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                </h2>
                                <p class="post_time">
                                    <?php the_time('j l в H:i'); ?>
                                </p>
                                <a href="<?php the_permalink(); ?>" class="post_link-img">
                                    <?php the_post_thumbnail(); ?>
                                </a>
                                <div class="post_prev-text">
                                    <!-- добовление короткого описание производится в админке (вверху справа, настройки екрана)-->
                                    <?php the_excerpt(); ?>
                                </div>
                            </div>


                        <?php endwhile; ?>
                    </div>

                    <div class="column_2">
                        <?php $i = 0; ?>
                        <?php while(have_posts()) : the_post(); $i++;?> <!-- Запускаем цикл перебора постов -->
                            <?php if ($i % 2) continue; ?>

                            <?php $category = get_the_category($post->ID)[0]; ?>

                            <div class="post <?php echo $a ?>">
                                <a href="<?= get_category_link($category->cat_ID)?>" style="background-image: url('<?= get_template_directory_uri();?>/images/<?= $category->cat_ID; ?>.png')" class="post_cat"></a>
                                <h2>
                                    <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                </h2>
                                <p class="post_time">
                                    <?php the_time('j l в H:i'); ?>
                                </p>
                                <a href="<?php the_permalink(); ?>" class="post_link-img">
                                    <?php the_post_thumbnail(); ?>
                                </a>
                                <div class="post_prev-text">
                                    <!-- добовление короткого описание производится в админке (вверху справа, настройки екрана)-->
                                    <?php the_excerpt(); ?>
                                </div>
                            </div>


                        <?php endwhile; ?>
                    </div>



                    <?php the_posts_pagination(); ?>

                <?php endif; ?>

            </div>
            <div class="col-right">

                <div class="sb_category">
                    <p class="sb_name">Категории</p>
                    <?php get_sidebar(); ?>
                </div>
                <div class="sb_vk">
                    <a href="#">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/logo_circle.png" alt="images" width="52" height="52" class="logo-circle">
                        <span>Подпишись на наше сообщество в vk!</span>
                        <img src="<?php echo get_template_directory_uri(); ?>/images/vk_line.png" alt="images" width="85" height="50" class="vk-line">
                    </a>
                </div>
                <div class="sb_inter-news">
                    <p class="sb_name">Новости интернета</p>
                </div>
                <div class="sb_music">
                    <p class="sb_name">Музыка</p>
                </div>

                <a href="#" class="up-to-header">

                </a>
            </div>

        </div>
    </main>
<?php get_footer(); ?>