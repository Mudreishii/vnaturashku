<?php

    // Добавит окно миниатюры постов в админку
    if ( function_exists( 'add_theme_support' ) ) add_theme_support( 'post-thumbnails' );

    //  Регистрируем два сайдбара
    register_sidebar( array(
        'id'          => 'top-menu',
        'name'        => __( 'dddd' ),
        'description' => __( 'This sidebar is located above the age logo.')
    ));
    register_sidebar( array(
        'id'          => 'bot-menu',
        'name'        => __( 'aaa' ),
        'description' => __( 'This sidebar is located above the age logo.')
    ));

    // Делаем возможность добовления категории к новым Страницам
    function true_apply_categories_for_pages(){
        add_meta_box( 'categorydiv', 'Категории', 'post_categories_meta_box', 'page', 'side', 'normal'); // добавляем метабокс категорий для страниц
        register_taxonomy_for_object_type('category', 'page'); // регистрируем рубрики для страниц
    }
    // обязательно вешаем на admin_init
    add_action('admin_init','true_apply_categories_for_pages');
    function true_expanded_request_category($q) {
        if (isset($q['category_name'])) // если в запросе присутствует параметр рубрики
            $q['post_type'] = array('post', 'page'); // то, помимо записей, выводим также и страницы
        return $q;
    }
    add_filter('request', 'true_expanded_request_category');

//    Автоматическая подгрузка постов 3 vs 3
    function true_load_posts(){
        $args = unserialize(stripslashes($_POST['query']));
        $args['paged'] = $_POST['page'] + 1; // следующая страница
        $args['post_status'] = 'publish';
        $q = new WP_Query($args);
        if( $q->have_posts() ):
            while($q->have_posts()): $q->the_post();
                ?>
                <?php $category = get_the_category($post->ID)[0]; ?>
                <div class="post" id="post-<?php echo $post->ID ?>">
                    <a href="<?= get_category_link($category->cat_ID)?>" style="background-image: url('<?= get_template_directory_uri();?>/images/<?= $category->cat_ID; ?>.png')" class="post_cat"></a>
                    <h2>
                        <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                    </h2>
                    <p class="post_time">
                        <?php the_time('j l в H:i'); ?>
                    </p>
                    <a href="<?php the_permalink(); ?>" class="post_link-img">
                        <?php the_post_thumbnail(); ?>
                    </a>
                    <div class="post_prev">
                        <!-- добовление короткого описание производится в админке (вверху справа, настройки екрана)-->
                        <?php the_excerpt(); ?>
                    </div>
                </div>
            <?php
            endwhile; endif;
        wp_reset_postdata();
        die();
    }
    add_action('wp_ajax_loadmore', 'true_load_posts');
    add_action('wp_ajax_nopriv_loadmore', 'true_load_posts');



?>